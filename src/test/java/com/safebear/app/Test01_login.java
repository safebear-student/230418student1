package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 23/04/2018.
 */
public class Test01_login extends BaseTest {


    @Test
    public void testLogin() {
        //Step 1 confirm we're on the welcome page
        assertTrue(welcomePage.CheckCorrectPage());

        //step 2 Click on the Login Link and the Login Page loads
        welcomePage.ClickOnLogin();

        // Step 3 Confirm that we're now on the Login Page
        assertTrue(loginPage.checkCorrectPage());

        //Step 4 Login with Valid Credentials
        loginPage.login("testuser","testing");

        //step 5 Check that we're now on the user page
        assertTrue(userPage.CheckCorrectPage());




    }

}